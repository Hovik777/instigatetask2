package com.example.loginregistrrecyclerview.activity;

import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;

import com.example.loginregistrrecyclerview.R;
import com.example.loginregistrrecyclerview.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getName();
    private ActivityMainBinding mainBinding;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        createFragment(R.id.main_container, new MainFragment());
    }

  public void createFragment(int conteinerId, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(conteinerId, fragment, fragment.getClass().getName())
                .addToBackStack("")
                .commit();
  }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_container);
        if(fragment == null) {
            finish();
        }
    }
}
