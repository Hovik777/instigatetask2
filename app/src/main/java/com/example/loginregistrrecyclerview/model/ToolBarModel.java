package com.example.loginregistrrecyclerview.model;

public class ToolBarModel {


    private String text;
    private int icon;

    public ToolBarModel(String text, int icon) {
        this.text = text;
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public int getIcon() {
        return icon;
    }

}
