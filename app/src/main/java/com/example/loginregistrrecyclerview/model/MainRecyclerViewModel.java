package com.example.loginregistrrecyclerview.model;

public class MainRecyclerViewModel {
    private String fullName;
    private String city;
    private String phone;
    private String state;
    private String imageUrl;

    public MainRecyclerViewModel(String fullName, String imageUrl, String state, String city, String phone) {
        this.fullName = fullName;
        this.city = city;
        this.phone = phone;
        this.state = state;
        this.imageUrl = imageUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
