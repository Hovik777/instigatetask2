package com.example.loginregistrrecyclerview.webservice;

import com.example.loginregistrrecyclerview.model.MAinResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("api?results=100&fbclid=IwAR1hZ1MlqLdnAk_aL6gvyT3kuSzl9a1ZqdWK05uK6qF28vEpeSdYEyUyVck")
    Call<MAinResponseModel> getUserData();
}
