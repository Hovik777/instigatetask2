package com.example.loginregistrrecyclerview.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.loginregistrrecyclerview.databinding.ViewItemDataBinding;
import com.example.loginregistrrecyclerview.model.MAinResponseModel;
import com.example.loginregistrrecyclerview.model.MainRecyclerViewModel;

import java.util.List;

public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.MyViewHolder> {

    private MAinResponseModel mAinResponseModel;
    private Context context;

    public MainRecyclerViewAdapter(MAinResponseModel mAinResponseModel, Context context) {
        this.mAinResponseModel = mAinResponseModel;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewItemDataBinding itemDataBinding = ViewItemDataBinding.inflate(inflater, parent, false);
        return new MyViewHolder(itemDataBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
      MAinResponseModel.Result result = mAinResponseModel.getResults().get(position);
      holder.dataBinding.setUser(new MainRecyclerViewModel(result.getName().getFirst(), result.getPicture().getMedium(),
              result.getLocation().getState(), result.getLocation().getCity(), result.getPhone()
              ));
      holder.dataBinding.getRoot().setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

          }
      });
    }
    private void openDetailScreen(View view, MAinResponseModel.Result data){

    }
    @Override
    public int getItemCount() {
        return mAinResponseModel.getResults().size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ViewItemDataBinding dataBinding;
        public MyViewHolder(View itemView) {
            super(itemView);
            dataBinding = DataBindingUtil.bind( itemView );
        }
    }
}
