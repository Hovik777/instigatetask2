package com.example.loginregistrrecyclerview.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.loginregistrrecyclerview.R;
import com.example.loginregistrrecyclerview.adapter.MainRecyclerViewAdapter;
import com.example.loginregistrrecyclerview.databinding.FragmentMainBinding;
import com.example.loginregistrrecyclerview.model.MAinResponseModel;
import com.example.loginregistrrecyclerview.model.ToolBarModel;
import com.example.loginregistrrecyclerview.webservice.ApiClient;
import com.example.loginregistrrecyclerview.webservice.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {
    // view
    private View view;
    // object
    private Context appContext;

    private RecyclerView mainRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private MainRecyclerViewAdapter adapter;
    private FragmentMainBinding dataBinding;


    public MainFragment() {
// Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appContext = context.getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main,container,false);
        view = dataBinding.getRoot();
        manageToolBar();
        dataBinding.setMainFragment(dataBinding.getMainFragment());
        mainRecyclerView = view.findViewById(R.id.mainRecyclerViewID);
        getUserData();
        return view;
    }

    private void manageToolBar() {
        dataBinding.setMainFragment(new ToolBarModel(getString(R.string.users), 0));
    }

    private void initAdapter(MAinResponseModel responseModel){
        linearLayoutManager = new LinearLayoutManager(appContext);
        mainRecyclerView.setLayoutManager(linearLayoutManager);
        mainRecyclerView.setAdapter( adapter );
        adapter = new MainRecyclerViewAdapter(responseModel,appContext);
        mainRecyclerView.setAdapter(adapter);

    }

    private void getUserData(){
        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<MAinResponseModel> call = apiService.getUserData();
        call.enqueue(new Callback<MAinResponseModel>() {
            @Override
            public void onResponse(Call<MAinResponseModel> call, Response<MAinResponseModel> response) {
             initAdapter(response.body());
            }

            @Override
            public void onFailure(Call<MAinResponseModel> call, Throwable t) {
                Log.d("TAG","Response = "+t.toString());
            }
        });
    }


}
